import requests
from bs4 import BeautifulSoup
import csv
import json

# wrote the grabbed data into csv file##########
# # make a get request usining requests
# req = requests.get('https://authoraditiagarwal.com/')
#
# # cretae a soup object
# soupobj = BeautifulSoup(req.text, 'lxml')

# # write the grabbed data into csv file
# f = csv.writer(open('dataprocessing.csv', "w"))
# f.writerow(['Title'])
# f.writerow([soupobj.title.text])
# wrote the grabbed data into csv file ends here########

# collecting data into json file ######################
r = requests.get('https://authoraditiagarwal.com/')
soup = BeautifulSoup(r.text, 'lxml')
y = json.dumps(soup.title.text)
with open('JSONFile.txt', 'wt') as outfile:
   json.dump(y, outfile)
# collecting data into json file ends here ######################

# https://www.tutorialspoint.com/python_web_scraping/python_web_scraping_data_processing.htm
# below two topics are pending
# Data Processing using AWS S3
# Data processing using MySQL
