import csv
from bs4 import BeautifulSoup as Soup
from urllib import urlopen

link = 'http://pci.nic.in/apps/FacultySearch/Teacher_Profile.aspx?TeacherID='
di = {'LPHC': {'start': 2007054, 'end': 2022519}, 'LPMM': {'start': 2051824, 'end': 2055710},
      'LPHA': {'start': 2000002, 'end': 2048922}, 'LPHL': {'start': 2018400, 'end': 2044043},
      'LPHP': {'start': 2007023, 'end': 2028021}, 'LPPL': {'start': 2024188, 'end': 2050003},
      'LPQA': {'start': 2052539, 'end': 2057387}}

with open('teacherId.csv', 'a') as f:
    writer = csv.writer(f)
    writer.writerow(
        ['NAME', 'TEACHID', 'COUNCIL', 'REGITRATIONNO', 'REGITRATIONDATE', 'GENDER', 'CASTE', 'DATE OF BIRTH',
         'FATHERNAME', 'COLLEGE NAME', 'DEPARTMENT', 'DATE OF JOINING', 'DESIGNATION', 'ADDRESS', 'CITY', 'STATE',
         'EMAIL', 'MOBILE', 'OFFICE PHONE', 'HOUSE PHONE', 'QUALIFICATION', 'EXPERIENCE'])


def wricsv(details):
    try:
        fields = ['NA' if field == '' else field for field in details]
        with open("teacherId.csv", 'a') as resultFile:
            wr = csv.writer(resultFile, dialect='excel')
            wr.writerow(fields)
    except:
        print('something gone wrong in writing csv')


def qual(soup):
    for i in soup.findAll('table', {'id': 'GrdQualification'}):
        k = bs(str(i), 'html.parser')
        for j in k.findAll('tr', {'class': 'tdrow'})[-1:]:
            qu = j.getText().encode('utf-8')
    return qu


def exp(soup):
    for i in soup.findAll('table', {'id': 'GridView1'}):
        for j in i.findAll('tr', {'class': 'tdrow'})[-1]:
            ex = j.getText().encode('utf-8')
    return ex


def readdata(text):
    soup = Soup(text)
    details = []
    if len(soup.find('span', {'id': 'lblFirstName'}).getText()) > 0:
        name = soup.find('span', {'id': 'lblFirstName'}).getText()
        print(name)
        teachId = soup.find('span', {'id': "lblTeacherID"}).getText()
        council = soup.find('span', {'id': "lblCouncil"}).getText()
        regNo = soup.find('span', {'id': "lblPrimaryRegNo"}).getText()
        regDate = soup.find('span', {'id': "lblRegDate"}).getText()
        gen = soup.find('span', {'id': "lblGender"}).getText()
        cast = soup.find('span', {'id': "lblCast"}).getText()
        dob = soup.find('span', {'id': "lblDOB"}).getText()
        fname = soup.find('span', {'id': "lblFatherName"}).getText()
        clgname = soup.find('span', {'id': "lblCollegeName"}).getText()
        dept = soup.find('span', {'id': "lblDept"}).getText()
        doj = soup.find('span', {'id': "lblDoj"}).getText()
        desig = soup.find('span', {'id': "lblDesignation"}).getText()
        addr = soup.find('span', {'id': "lblCurrAdd1"}).getText()
        city = soup.find('span', {'id': "lblCurrCity"}).getText()
        state = soup.find('span', {'id': "lblCurrState"}).getText()
        mob = soup.find('span', {'id': "lblMobileNumber"}).getText()
        teleo = soup.find('span', {'id': "lblCurrTelephoneOff"}).getText()
        teler = soup.find('span', {'id': "lblCurrTelephoneR"}).getText()
        email = soup.find('span', {'id': "lblEmail"}).getText()
        if len(soup.findAll('table', {'id': 'GridView1'})) > 1:
            experience = exp(soup)
        else:
            experience = 'NA'

        if len(soup.findAll('span', {'id', 'GrdQualification'})) > 1:
            qualification = qual(soup)
        else:
            qualification = 'NA'

        details.extend(
            [name, teachId, council, regNo, regDate, gen, cast, dob, fname, clgname, dept, doj, desig, addr, city,
             state, email, mob, teleo, teler, qualification, experience])
        wricsv(details)
    else:
        print('invalid teacherId')


def hiturl(url, start, stop):
    for num in range(start, stop + 1):
        http = url + str(num)
        print(num)
        text = urlopen(http).read()
        readdata(text)


for i in di:
    series = i
    url = link + series + '-'
    start = di[i]['start']
    stop = di[i]['end']
    hiturl(url, start, stop)
