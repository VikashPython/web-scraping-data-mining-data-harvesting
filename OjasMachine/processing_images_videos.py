import requests

# url to download media content
url = "https://authoraditiagarwal.com/wpcontent/uploads/2018/05/MetaSlider_ThinkBig-1080x180.jpg"

# request obj
r = requests.get(url)

# save the recieved content
# with open("ThinkBig.png", 'wb') as f:
#     f.write(r.content)
# -----------------------------------------

# Extracting Filename from URL-----------------
import urllib3
import os
from urllib.parse import parse_qsl, urljoin, urlparse

url = "https://authoraditiagarwal.com/wpcontent/uploads/2018/05/MetaSlider_ThinkBig-1080x180.jpg"
a = urlparse(url)
# print(a.path)
# print(os.path.basename(a.path))
# ----------------------------------------

# Information about Type of Content from URL
# CRETAE RESPONSE OBJECT
robj = requests.get(url, allow_redirects=True)
for headers in robj.headers:
    # print(headers)
    pass

# get particular information
# print('--------------')
# print(robj.headers.get('content-type'))

# Generating Thumbnail for Images ##############
import glob
from PIL import Image
for infile in glob.glob("vikash.png"):
   img = Image.open(infile)
   img.thumbnail((128, 128), Image.ANTIALIAS)
   if infile[0:2] != "Th_":
      img.save("Th_" + infile, "png")

# Screenshot from Website ############
from selenium import webdriver
path = r'E:\VIkash\app\chromedriver_win32\chromedriver'
# browser = webdriver.Chrome(executable_path=path)
# browser.get('http://tutorialspoint.com/')
# screenshot = browser.save_screenshot('screenshot.png')
# browser.quit()
# Thumbnail Generation for Video ##########
# pending
# Ripping an MP4 video to an MP3 #############
import moviepy.editor as mp
clip = mp.VideoFileClip(r"WhatsApp.mp4")
clip.audio.write_audiofile("movie_audio.mp3")
