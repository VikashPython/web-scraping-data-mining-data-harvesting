from bs4 import BeautifulSoup
import requests

url = "http://example.com/"

# Getting the webpage, creating a Response object.
response = requests.get(url)
# print("response-->", response)
# Extracting the source code of the page.
data = response.text
headerdata = response.headers
print(data)
# Passing the source code to Beautiful Soup to create a BeautifulSoup object for it.
soup = BeautifulSoup(data, 'html.parser')
# print(soup)
# Extracting all the <a> tags into a list.
tags = soup.find_all('a')
print(tags)
# Extracting URLs from the attribute href in the <a> tags.
for tag in tags:
    print(tag.get('href'))
