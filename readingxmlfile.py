from bs4 import BeautifulSoup as bs


def read_file():
    file = open('vikash.xml', encoding="utf")
    data = file.read()
    file.close()
    return data

# Make soup
# Syntax = BeautifulSoup(html_data,parser)
# Our parser is lxml or html.parser which we have installed


html_file = read_file()


soup = bs(html_file, 'lxml')
# tags = soup.find_all("draw", {"name":"frame"})
tags = soup.find_all("books")
# or for those who haven't installed lxml - BeautifulSoup(html_file,'html.parser')
print(len(tags))
# soup prettify
# print(soup.prettify())
print(tags)
