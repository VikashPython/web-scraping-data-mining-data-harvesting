import requests
from fake_useragent import UserAgent
from bs4 import BeautifulSoup
url = 'https://ojas-it.com/team.php'
ojas_page = requests.get(url)
soup = BeautifulSoup(ojas_page.content, 'lxml')

all_divs = soup.find_all('div', attrs={'class':'item'})
members = [div.ul.li.h3.string for div in all_divs]
print('Ojas Team:')
for i in members:
    print(i)
